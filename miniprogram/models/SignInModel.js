
import { CloudRequest } from '../utils/customer-request.js'
class SignInModel extends CloudRequest {
  
  /**
   * 根据商品ID 获取商品信息
   * @param {*} product_id 
   * @param {*} callBack 
   */
  getCalendar(calendar, callBack) {
    this.request({
      url: "getCalendar",
      data:{calendar:calendar},
      success: res => {
        callBack(res)
      }
    })
  }
  updateCalendar(calendar, calendarId, callBack) {
    this.request({
      url: "updateCalendar",
      data:{
        calendar : calendar,
        calendarId: calendarId
        },
      success: res => {
        callBack(res)
      }
    })
  }

  addCalendar(calendar, callBack) {
    this.request({
      url: "addCalendar",
      data:{
        calendar : calendar,
        },
      success: res => {
        callBack(res)
      }
    })
  }


 
}
export { SignInModel }
