
import { CloudRequest } from '../utils/cloud-request.js'
class PrinterModel extends CloudRequest {
  
  /**
   * 根据商品ID 获取商品信息
   * @param {*} product_id 
   * @param {*} callBack 
   */
  getPrinter(callBack) {
    this.request({
      url: "getPrinter",
      success: res => {
        callBack(res)
      }
    })
  }
  updatePrinter(printer, printerId, callBack) {
    this.request({
      url: "updatePrinter",
      data:{
          printer : printer,
          printerId: printerId
        },
      success: res => {
        callBack(res)
      }
    })
  }
  getClientId(){
    this.request({
        url: "getPrinter",
        success: res => {
          return res
        }
      })
  }

 
}
export { PrinterModel }
