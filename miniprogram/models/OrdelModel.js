import { CloudRequest } from '../utils/cloud-request.js'
class OrderModel extends CloudRequest {

    /**
     * 生成订单
     * @param {*} orderData 
     * @param {*} callBack 
     */
    creat(orderData, callBack) {
        this.request({
            url: "creatOrder",
            data: { orderData: orderData },
            success: res => {
                callBack(res)
            }
        })
    }

    /**
     * 根据订单id查询
     * @param {*} orderId 
     * @param {*} callBack 
     */
    getOrderById(orderId, callBack) {
        this.request({
            url: "getOrderById",
            data: { orderId: orderId },
            success: res => {
                callBack(res)
            }
        })
    }

    /**
     * 查询订单
     * @param {*} callBack 
     */
    getOrderList(openId, callBack) {
        this.request({
            url: "getOrderList",
            data:{openId:openId},
            success: res => {
                callBack(res)
            }
        })
    }
    // 获取所以订单
    getOrders( callBack) {
        this.request({
            url: "getOrders",
            success: res => {
                callBack(res)
            }
        })
    }
    /**
     * 查询未付款订单
     * @param {*} callBack 
     */
    getPendingOrder(callBack){
        this.request({
            url: "getPendingOrder",
            success: res => {
                callBack(res)
            }
        })
    }

    delOrder(orderId,callBack){
        this.request({
            url: "delOrder",
            data:{orderId:orderId},
            success: res => {
                callBack(res)
            }
        })
    }
    updateOrder(orderId, order, callBack){
        this.request({
            url:"updateOrder",
            data:{
                orderId:orderId,
                order:order
            },
            success: res=> {
                callBack(res)
            }
        })
    }


}

export { OrderModel }