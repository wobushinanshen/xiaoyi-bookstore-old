
import { CloudRequest } from '../utils/cloud-request.js'
class NoticeModel extends CloudRequest {
  
  /**
   * 根据商品ID 获取商品信息
   * @param {*} product_id 
   * @param {*} callBack 
   */
  getNotice(callBack) {
    this.request({
      url: "getNotice",
      success: res => {
        callBack(res)
      }
    })
  }
  getNoticeById(noticeId, callBack) {
    this.request({
      url: "getNoticeById",
      data:{noticeId:noticeId},
      success: res => {
        callBack(res)
      }
    })
  }

  updateNotice(notice, noticeId, callBack) {
    this.request({
      url: "updateNotice",
      data:{
        notice:notice,
        noticeId:noticeId
      },
      success: res => {
        callBack(res)
      }
    })
  }
  delNotice(noticeId, callBack){
    this.request({
        url: "delNotice",
        data:{noticeId:noticeId},
        success: res => {
          callBack(res)
        }
      })
  }
 
}
export { NoticeModel }
