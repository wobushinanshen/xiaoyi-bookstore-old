import { CloudRequest } from '../utils/customer-request.js'
class CouponModel extends CloudRequest {
    /**
     * 获取商家账户信息
     * 
     */
    getCoupon(coupon, callBack) {
        this.request({
            url: "getCoupon",
            data:{
                coupon:coupon
            },
            success: res => {
                callBack(res)
            }
        })
    }
    updateCoupon(coupon, couponId, callBack){
        this.request({
            url:"updateCoupon",
            data:{
                coupon:coupon,
                couponId:couponId
            },
            success: res => {
                callBack(res)
            }
        })
    }
    addCoupon(coupon, callBack){
        this.request({
            url:"addCoupon",
            data:{
                coupon:coupon,
            },
            success: res => {
                callBack(res)
            }
        })
    }
    delCoupon(couponId, callBack){
        this.request({
            url:"delCoupon",
            data:{
                couponId:couponId
            },
            success: res => {
                callBack(res)
            }
        })
    }

    
}

export { CouponModel }
