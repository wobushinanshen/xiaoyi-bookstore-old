
import { CloudRequest } from '../utils/cloud-request.js'
class ProductModel extends CloudRequest {
  
  /**
   * 根据商品ID 获取商品信息
   * @param {*} product_id 
   * @param {*} callBack 
   */
  getProductById(product_id,callBack) {
    this.request({
      url: "getProductById",
      data:{product_id:product_id},
      success: res => {
        callBack(res)
      }
    })
  }
  /**
   * 模糊查询
   * @param {*} regexp 
   * @param {*} callBack 
   */
  regQuery(regexp, columns, callBack) {
    this.request({
      url: "regQuery",
      data:{
        regexp:regexp,
        columns:columns
      },
      success: res => {
        callBack(res)
      }
    })
  }

  /**
   * 根据主题类型获取商品信息
   * @param {*} theme_type 
   * @param {*} callBack 
   */
  getThemeProduct(theme_type,callBack){
    theme_type = parseInt(theme_type) 
    this.request({
      url:"getThemeProduct",
      data:{theme_type:theme_type},
      success:res=>{
        callBack(res)
      }
    })
  }
  /**
   * 增加商品
   * @param {z} product 
   * @param {*} callBack 
   */
  addProduct(product,callBack){
    this.request({
      url: "addProduct",
      data: {product:product},
      success:res=>{
        callBack(res)
      }

    })
  }

  getProduct(callBack){
    this.request({
      url: "getProduct",
      success:res=>{
        callBack(res)
      }

    })
  }

  updateProduct(product,productId,callBack){
    this.request({
      url: "updateProduct",
      data: {product:product,
            productId: productId
      },
      success:res=>{
        callBack(res)
      }

    })
  }

  delProduct(productId,callBack){
    this.request({
      url: "delProduct",
      data: { productId: productId},
      success:res=>{
        callBack(res)
      }

    })
  }


  

}
export { ProductModel }
