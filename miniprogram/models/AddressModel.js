
import { CloudRequest } from '../utils/cloud-request.js'
class AddressModel extends CloudRequest {
  
  /**
   * 根据商品ID 获取商品信息
   * @param {*} product_id 
   * @param {*} callBack 
   */
  
  getAddressList(openId,callBack) {
    this.request({
      url: "getAddressList",
      data:{openId:openId},
      success: res => {
        callBack(res)
      }
    })
  }
  getAddressById(addressId, callBack) {
    this.request({
      url: "getAddressById",
      data:{addressId:addressId},
      success: res => {
        callBack(res)
      }
    })
  }
  addAddress(openId,address, callBack){
    this.request({
        url: "addAddress",
        data:{
          address:address,
          openId:openId
        },
        success: res => {
          callBack(res)
        }
      })
  }

  resetAddress(openId,callBack){
    this.request({
        url: "resetAddress",
        data:{openId:openId},
        success: res => {
          callBack(res)
        }
      })
  }

  modifyAddress(address, addressId, callBack){
    this.request({
        url: "modifyAddress",
        data:{
          address : address,
          addressId : addressId
        },
        success: res => {
          callBack(res)
        }
      })
  }
  delAddress(addressId, callBack){
    this.request({
        url: "delAddress",
        data:{addressId:addressId},
        success: res => {
          callBack(res)
        }
      })
  }

  getOpenId(callBack){
    this.request({
        url: "getOpenId",
        success: res => {
          callBack(res)
        }
      })
  }
  




}
export { AddressModel }
