
import { CloudRequest } from '../utils/cloud-request.js'
class AreaModel extends CloudRequest {
  
  /**
   * 根据商品ID 获取商品信息
   * @param {*} product_id 
   * @param {*} callBack 
   */
  getAreaList(callBack) {
    this.request({
      url: "getAreaList",
      success: res => {
        callBack(res)
      }
    })
  }

}
export { AreaModel }
