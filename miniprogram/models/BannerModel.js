import { CloudRequest } from '../utils/cloud-request.js'
class BannerModel extends CloudRequest {
    /**
     * 获取分类
     * @param {*} callBack 
     */
    getBanner(callBack){
        this.request({
            url: "getBanners",
            success: res => {
              callBack(res)
            }
        })
    }

    addBanner(banner,callBack){
        this.request({
            url: "addBanner",
            data:{banner:banner},
            success: res => {
              callBack(res)
            }
        })
    }

    delBanner(bannerId,callBack){
        this.request({
            url: "delBanner",
            data:{bannerId:bannerId},
            success: res => {
              callBack(res)
            }
        })
    }

}
export { BannerModel }