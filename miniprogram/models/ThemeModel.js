import { CloudRequest } from '../utils/cloud-request.js'
class ThemeModel extends CloudRequest {
    /**
     * 获取分类
     * @param {*} callBack 
     */
    getTheme(callBack){
        this.request({
            url: "getTheme",
            success: res => {
              callBack(res)
            }
        })
    }
    
    /**
     * 根据商品类型获取商品
     * @param {*} category_type 
     * @param {*} callBack 
     */
    getThemeProduct(theme_type,callBack){
        this.request({
            url: "getThemeProduct",
            data:{theme_type:theme_type},
            success: res => {
              callBack(res)
            }
        })
    }

    addTheme(theme,callBack){
        this.request({
            url: "addTheme",
            data:{theme:theme},
            success: res => {
              callBack(res)
            }
        })
    }

    delTheme(themeId,callBack){
        this.request({
            url: "delTheme",
            data:{theme_id:themeId},
            success: res => {
              callBack(res)
            }
        })
    }

}
export { ThemeModel }