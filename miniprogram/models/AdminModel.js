import { CloudRequest } from '../utils/cloud-request.js'
class AdminModel extends CloudRequest {
    /**
     * 获取商家账户信息
     * 
     */
    getAdmin(callBack) {
        this.request({
            url: "getAdmin",
            success: res => {
                callBack(res)
            }
        })
    }
    updateAdmin(admin, adminId, callBack){
        this.request({
            url:"updateAdmin",
            data:{
                admin:admin,
                adminId:adminId
            },
            success: res => {
                callBack(res)
            }
        })
    }

    
}

export { AdminModel }
