import { CloudRequest } from '../utils/customer-request.js'
class ShopModel extends CloudRequest {

   
    // 获取所以订单
    getShop( shop, callBack) {
        this.request({
            url: "getShop",
            data:{shop:shop},
            success: res => {
                callBack(res)
            }
        })
    }
  

    delShop(shopId,callBack){
        this.request({
            url: "delShop",
            data:{shopId:shopId},
            success: res => {
                callBack(res)
            }
        })
    }
    updateShop(shop, shopId, callBack){
        this.request({
            url:"updateShop",
            data:{
                shopId:shopId,
                shop:shop
            },
            success: res=> {
                callBack(res)
            }
        })
    }


}

export { ShopModel }