import { CloudRequest } from '../utils/customer-request.js'
class CustomerModel extends CloudRequest {
    /**
     * 获取商家账户信息
     * 
     */
    getCustomer(customer, callBack) {
        this.request({
            url: "getCustomer",
            data:{
                customer:customer
            },
            success: res => {
                callBack(res)
            }
        })
    }
    updateCustomer(customer, customerId, callBack){
        this.request({
            url:"updateCustomer",
            data:{
                customer:customer,
                customerId:customerId
            },
            success: res => {
                callBack(res)
            }
        })
    }
    addCustomer(customer, callBack){
        this.request({
            url:"addCustomer",
            data:{
                customer:customer,
            },
            success: res => {
                callBack(res)
            }
        })
    }
    delCustomer(customerId, callBack){
        this.request({
            url:"delCustomer",
            data:{
                customerId:customerId
            },
            success: res => {
                callBack(res)
            }
        })
    }

    
}

export { CustomerModel }
