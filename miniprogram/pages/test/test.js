import { SignInModel } from '../../models/SignInModel.js'
import { CustomerModel } from '../../models/CustomerModel.js'
var zhenzisms = require('../../utils/zhenzisms.js');
let SignIn = new SignInModel()
let Customer = new CustomerModel()
let App = getApp()
const date = new Date()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        days_color:[],
        calendar:{},
        disabled:false,
        openId:'',
        customer:{},
        code:'',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        
    },
    send:function(){
        let apiUrl = 'https://sms_developer.zhenzikj.com'
        let appId = '107324'
        let appSecret = '963ccb12-5b98-4103-8eac-047e1deb7489'
        zhenzisms.client.init(apiUrl, appId, appSecret);
        var params = {};
        params.number = '13253377401';
        params.templateId = '2532';
        var code = zhenzisms.client.createCode(4, 60, params.number);//生成验证码
        var templateParams = [code, '5分钟'];
        params.templateParams = templateParams;
        // params.messageId = '1111111';
        // params.clientIp = '221.221.221.111';
        // console.log(code);
        zhenzisms.client.send(function (res) {
          wx.showToast({
            title: res.data.data,
            icon: 'none',
            duration: 2000
          })
        }, params);
    
    },
    validateCode:function(){
        let number = '13253377401'
        let code  = this.data.code
        var result = zhenzisms.client.validateCode(number, code);
        switch(result){
            case 'ok':{
                console.log('校验成功')
                break
            }
            case 'code_error':{
                console.log('验证码不一致') 
                break
            }
            case 'code_expired':{
                console.log('已过期')
                break
            }
            default:{
                console.log('其他')
                break
            }
               
        }

    },
    inputCode:function(e){
        let code = e.detail.value
        this.setData({code})
    },  

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})