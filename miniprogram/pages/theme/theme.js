// pages/theme/theme.js
import {  ProductModel } from '../../models/productModel.js'
let productModel = new ProductModel();
import { CartModel } from '../../models/CartModel.js'
let Cart = new CartModel()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    products:[],
    product:{},
    show:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('themeopptions',options)
    this._init(options.theme_type)
  },
  // 初始化
  _init:function(theme_type){
    // 根据类型获取商品
    productModel.getThemeProduct(parseInt(theme_type),res=>{
      console.log('book',res)
      this.setData({
        products : res.result.data.data
      })
    })
  },
   // 图书购物车图标
   addCart:function(e){
    let show = e.detail.show
    let product = e.detail.product
    console.log('e',e.detail)
    this.setData({
      show:show,
      product:product
    })
  },
  joinCart:function(e){
    let count = e.detail.count
    this.setData({
      show:true
    })
    Cart.add(this.data.product,count,res=>{
      this.setData({
        show:false
      })
      wx.showToast({
        icon:"success",
        context:"添加成功",
        title:'添加成功'
      })
    })
  },
   // 跳转商品详情
   productDetails:function(e){
    //TODO: 跳转详情
    wx.navigateTo({
      url: '/pages/product/product?product_id=' + e.detail.productId,
    })
  }
  
})
