// pages/06-demo/06-demo.js
// 引入我们需要的类 
import { AdminModel } from "../../models/AdminModel.js"
// 使用钱需要实例化才能使用
let Admin = new AdminModel()

Page({
    data: {
        password:"",
        account:"",
        disabled:true,
        rememberPwd:false,
        admin:{},
        accountError:'',
        passwordError:'',
        checked:false

    },
    onLoad: function (options) {
        this.checkAutoLogin()
        this.checkInput()
        this._init()
    },
    changeCheck:function(e){
        this.setData({
            checked:!this.data.checked
        })
    },
    _init:function(){
        Admin.getAdmin(res => {
            this.setData({
                admin: res.result.data.data[0]
            })
        });
    },
    checkAutoLogin:function(){
        let admin = wx.getStorageSync('admin')
        if(admin){
            this.setData({
                account:admin.account,
                password:admin.password,
                checked:true
            })
        }else{
            console.log('no admin')
            this.setData({
                checked:false
            })
        }
    },
    RememberPwd:function(evt){
        if(this.data.checked){
            let admin = {
                account:this.data.account,
                password:this.data.password
            }
            wx.setStorageSync('admin', admin)
        }else{
            wx.removeStorage({
                key: 'admin',
                success (res) {
                }
              })
        }
    },
    checkInput:function(){
        if(this.data.account.length > 0 && this.data.password.length > 0){
            this.setData({
                disabled:false
            })
        }else{
         this.setData({
             disabled:true
         })
        }
    },
    checkAccount:function(){
       if(this.data.account.length <= 0){
           this.setData({
            accountError:'用户名不能为空'
           })
       }else{
        this.setData({
            accountError:''
           })
       }
    },
    checkPassword:function(){
        if(this.data.password.length <= 0){
            this.setData({
             passwordError:'密码不能为空'
            })
        }else{
         this.setData({
             passwordError:''
            })
        }
     },
    Submit:function(evt){
        this.RememberPwd()
        if(this.data.account == this.data.admin.account && this.data.password == this.data.admin.password){
            wx.redirectTo({
                url: '../admin/index/index'
            })
        }else{
            wx.showToast({
              title: '用户名或密码错误',
              icon: 'none'
            })
            this.onLoad()
        }
    },
    inputAccount:function(e){
        let account = e.detail
        this.setData({account})
        this.checkAccount()
        this.checkInput()
       
    },
    inputPassword:function(e){
        let password = e.detail
        this.setData({
            password:password
        })
        this.checkPassword()
        this.checkInput()
    }, 
 
})