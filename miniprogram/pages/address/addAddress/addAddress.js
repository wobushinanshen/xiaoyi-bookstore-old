import { AreaModel }  from '../../../models/AreaModel.js'
import { AddressModel }  from '../../../models/AddressModel.js'

import Toast from '../../../components/dist/toast/toast';
let Area = new AreaModel()
let Address = new AddressModel()
var App = getApp()
Page({
    /**
     * 页面的初始数据
     */
    data: {
        areaList :{},
        show:false,
        address:"",
        area:"",
        detail:"",
        telephone:"",
        name:"",
        disabled :true,
        checked:true
    },
     //提交表单
     _submit:function(e){
        console.log('e',e.detail.value)
        let address = e.detail.value
        let openId = App.globalData.openId
        if(address.default){
            Address.resetAddress(openId,res=>{
                console.log('重置成功',res)
                Address.addAddress(openId,address,res=>{
                    console.log('添加成功',res)
                    Toast.success('添加成功')
                    //返回地址列表页面
                    // wx.navigateTo({
                    //   url: '../addressList/addressList',
                    // })
                    wx.navigateBack({
                        delta: 1,
                      })
                })
            })
           
          
        }else{
            Address.addAddress(openId,address,res=>{
                console.log('添加成功',res)
                Toast.success('添加成功')
                //返回地址列表页面
                wx.navigateBack({
                    delta: 1,
                  })
            })
        }
    },

    //点击确定事件 省市区选择弹出框
    confirm: function(e){
        let areaList = e.detail.values
        let address = ""
        for(var i in areaList){
            address += areaList[i].name
        }
        this.setData({ 
            show: false ,
            area:address
        });
    },
    //显示弹出框
    selectArea:function(){
        this.setData({ show: true });
    },
    //取消按钮 隐藏弹出框
    cancel:function(){
        this.setData({ show: false });
    },
    //判断输入内容是否为空 
    _isDisabled:function(){
        let area = this.data.area
        let detail = this.data.detail
        let telephone = this.data.telephone
        let name = this.data.name
        if(area !== "" && detail !== "" && telephone !== "" && name !== ""){
            this.setData({
                disabled:false
            })
        }else{
            this.setData({
                disabled:true
            })
        }
    },
    //将输入框内容赋值
    // 1、 姓名
    changeName:function(event){
        this.setData({
            name: event.detail
        })
        this._isDisabled()
    },
    //2、联系方式
    changeTelephone:function(event){
        this.setData({
            telephone: event.detail
        })
        this._isDisabled()
    },
    //3、详细地址
    changeDetail:function(event){
        this.setData({
            detail: event.detail
        })
        this._isDisabled()
    },
    onChange({ detail }) {
        // 需要手动对 checked 状态进行更新
        this.setData({ checked: detail });
      },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        let openId = App.globalData.openId
        console.log('openid',openId)
        Area.getAreaList(res=>{
            let areaList = res.result.data.data[0]
            console.log('address',res)
            this.setData({
                areaList:areaList
            })
        })
    },

  
})