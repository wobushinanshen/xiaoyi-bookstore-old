import { AreaModel }  from '../../../models/AreaModel.js'
import { AddressModel }  from '../../../models/AddressModel.js'
import Toast from '../../../components/dist/toast/toast';
let Area = new AreaModel()
let Address = new AddressModel()
var App = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        areaList :{},
        show:false,
        address:{},
        area:"",
        detail:"",
        telephone:"",
        name:"",
        disabled :false,
        checked:true,
        isDelete : 1
    },
     //提交表单
     _submit:function(e){
        let address = e.detail.value
        let addressId = this.data.address._id
        let openId = App.globalData.openId
        console.log('openId',openId)
        //判断是否修改为默认地址
        if(address.default){
            //若是：1、 先重置地址信息
            Address.resetAddress(openId,res=>{
                console.log('重置成功',res)
               
                //2、 更新地址信息
                Address.modifyAddress(address ,addressId ,res=>{
                console.log('modify',res)
                Toast.success('修改成功')
                // wx.navigateTo({
                //   url: '../addressList/addressList',
                // })
                wx.navigateBack({
                  delta: 1,
                })
                })
            })
        }else{
            Address.modifyAddress(address ,addressId ,res=>{
                console.log('modify',res)
                Toast.success('修改成功')
                wx.navigateBack({
                    delta: 1,
                  })
            })
        }
    },
    //删除地址
    delAddress:function(){
        let addressId = this.data.address._id
        Address.delAddress(addressId,res=>{
            console.log('del',res)
            Toast.success('删除成功')
        })
        wx.navigateBack({
          delta: 1,
        })
    },
    //点击确定事件 省市区选择弹出框
    confirm: function(e){
        let areaList = e.detail.values
        let address = ""
        for(var i in areaList){
            address += areaList[i].name
        }
        this.setData({ 
            show: false ,
            area:address
        });
    },
    //显示弹出框
    selectArea:function(){
        this.setData({ show: true });
    },
    //取消按钮 隐藏弹出框
    cancel:function(){
        this.setData({ show: false });
    },
    //判断输入内容是否为空 
    _isDisabled:function(){
        let area = this.data.address.area
        let detail = this.data.address.detail
        let telephone = this.data.address.telephone
        let name = this.data.address.name
        if(area !== "" && detail !== "" && telephone !== "" && name !== ""){
            this.setData({
                disabled:false
            })
        }else{
            this.setData({
                disabled:true
            })
        }
    },
    //将输入框内容赋值
    // 1、 姓名
    changeName:function(event){
        this.setData({
            'address.name': event.detail
        })
        this._isDisabled()
    },
    //2、联系方式
    changeTelephone:function(event){
        this.setData({
            'address.telephone': event.detail
        })
        this._isDisabled()
    },
    //3、详细地址
    changeDetail:function(event){
        this.setData({
            'address.detail': event.detail
        })
        this._isDisabled()
    },
    onChange({ detail }) {
        // 需要手动对 checked 状态进行更新
        this.setData({ 'address.default': detail });
      },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let isDelete = options.isDelete
        if(isDelete == 0){
            this.setData({
                isDelete:0
            })
        }
        this._init(options.addressId)
        console.log('option',options.addressId)
    },
    _init:function(addressId){
        Area.getAreaList(res=>{
            let areaList = res.result.data.data[0]
            this.setData({
                areaList:areaList
            })
        })
        Address.getAddressById(addressId,res=>{
            this.setData({
                address:res.result.data.data
            })
        })
    },


    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {


    },

  
})