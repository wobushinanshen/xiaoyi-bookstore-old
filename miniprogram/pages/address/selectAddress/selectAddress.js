// miniprogram/pages/address/addressList/addressList.js
let { AddressModel } = require('../../../models/AddressModel.js')
const Address = new AddressModel()
var App = getApp()
Page({
    /**
     * 页面的初始数据
     */
    data: {
        addressList:[],
        address:{},
        openid :""

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },

    _init:function(){
        let openId = App.globalData.openId
        Address.getAddressList(openId,res=>{
            this.setData({
                addressList:res.result.data.data
            })
            console.log('addressList',this.data.addressList)
        })
       

    },
    editAddress:function(e){
        let addressId = e.detail.addressId
        wx.navigateTo({
          url: '../editAddress/editAddress?addressId='+addressId,
        })
        console.log('e',addressId)
    },
    addAddress:function(){
        wx.navigateTo({
          url: '../addAddress/addAddress',
        })
    },
    selectAddress:function(e){
        let addressId = e.detail.addressId
        console.log('id',addressId)

        //获取当前页面路由栈的信息
        var pages = getCurrentPages();
       
         var prevPage = pages[pages.length -2 ];  //上一个页面
         //var nowPage = pages[pages.length -1];  //当亲页面
         console.log('pages',prevPage)
      

        //直接调用上一个页面的setData()方法，把数据存到上一个页面中去
        Address.getAddressById(addressId,res=>{
            let address = res.result.data.data
            console.log('address',address)
            prevPage.setData({
                address:address
            })
            wx.navigateBack({
                delta: 1,
              })
        })
       
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let openId = App.globalData.openId
        Address.getAddressList(openId, res=>{
            this.setData({
                addressList:res.result.data.data
            })
            console.log('addressList',this.data.addressList)
        })
    },

   
})