// miniprogram/pages/address/addressList/addressList.js
let { AddressModel } = require('../../../models/AddressModel.js')
const Address = new AddressModel()
var App = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        addressList:[],
        address:{}

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },

    _init:function(){
        console.log('globaldata',App.globalData.openId)
        let openId = App.globalData.openId
        Address.getAddressList(openId,res=>{
            this.setData({
                addressList:res.result.data.data
            })
            console.log('addressList',res)
        })
    },
    editAddress:function(e){
        let addressId = e.detail.addressId
        wx.navigateTo({
          url: '../editAddress/editAddress?addressId='+addressId,
        })
        console.log('e',addressId)
    },
    addAddress:function(){
        wx.navigateTo({
          url: '../addAddress/addAddress',
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
      this._init()
    },

})