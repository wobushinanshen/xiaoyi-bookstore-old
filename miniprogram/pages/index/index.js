import { NoticeModel } from '../../models/NoticeModel.js'
import { IndexModel } from "../../models/IndexModel.js"
import { ThemeModel } from '../../models/ThemeModel.js'
import { AddressModel }  from '../../models/AddressModel.js'
import { BannerModel } from '../../models/BannerModel.js'
import { CartModel } from '../../models/CartModel.js'
import { ProductModel } from '../../models/productModel.js'
let Product = new ProductModel()
let Banner  = new BannerModel()
let Address = new AddressModel()
let Notice = new NoticeModel()
let Theme = new ThemeModel()
var  App =getApp()
let Cart = new CartModel()
// 使用前需要实例化才能使用
let index = new IndexModel()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //自动轮播
    interval: 3000, // 自动切换时间间隔
    duration: 1000, // 滑动动画时长
    circular: true,//是否采用衔接滑动 
    disabled:true,//是否显示公告
    notices:[],//公告内容
    themes:[],
    banners:[],
    products:[],
    show:false,
    product:{},
    search:'',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
    this._init()
  },
  onShow:function(){
    this.setData({
      show:false
    })
  },
  _init:function(){ 
    // 1、用户授权
    this._getSetting()
    // 2、调用云函数获取openId
    wx.cloud.callFunction({
      name: 'login',
    }).then(res => {
      let openId = res.result.event.userInfo.openId
      App.globalData.openId = openId
    }).catch(err => {
    
    })
    // 轮播图 
    Banner.getBanner(res => {
      this.setData({
        banners :  res.result.data.data
      })
    })
    //公告
    Notice.getNotice(res=>{
      this.setData({
          notices:res.result.data.data,
      })
    })
    // 主题
    Theme.getTheme(res =>{
      this.setData({
        themes :  res.result.data.data
      })
    })
    // 最新商品
    index.getProductNew(res=>{
      this.setData({
        products :  res.result.data.data
      })
    })
  },
  searchInput:function(e){
    let search = e.detail
    this.setData({search})
   
  },
  onClick:function(){
    this.regexpSearch()
  },
  onSearch:function(){
    this.regexpSearch()
  },

  regexpSearch:function(){
    wx.navigateTo({
      url: '../search/search?search='+this.data.search,
    })
  },
  _getSetting:function(){
    // 获取用户已授权信息
    wx.getSetting({
      success(res) {
        // 如果尚未授权‘用户信息’
        if (!res.authSetting['scope.userInfo']) {
          // 提前向用户发起授权请求
          wx.authorize({
            scope: 'scope.userInfo',
            success () {
            
            }
          })
        }
      }
    }) 
  },
  joinCart:function(e){
    let count = e.detail.count
    this.setData({
      show:true
    })
    Cart.add(this.data.product,count,res=>{
      this.setData({
        show:false
      })
      wx.showToast({
        icon:"success",
        context:"添加成功",
        title:'添加成功'
      })
    })
  },
  // 点击购物车图标，弹出框
  addCart:function(e){
    let show = e.detail.show
    let product = e.detail.product
    this.setData({
      show:show,
      product:product
    })
  },
  // 弹出框，立即购买
  buy:function(e){
    let count = e.detail.count
    wx.navigateTo({
      url: '../order/order?count='+count+'&from=product'+'&productId='+this.data.product._id,
    })
  },
  
  themeNavigation: function (event) {
    console.log(event,'event')
    let theme_type = index.getDataSet(event, 'theme_type')
    wx.navigateTo({
      url: '../theme/theme?theme_type=' + theme_type,
    })
  },
  // 跳转商品详情
  showDetail: function (e) {
    this._navProductDetail(e.detail.productId)
  },
  productBanner: function (event) {
    let product_id = index.getDataSet(event, "productid")
    this._navProductDetail(product_id)
  },
  // 跳转详情
  _navProductDetail: function (product_id) {
    wx.navigateTo({
      url: '/pages/product/product?product_id=' + product_id,
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad()
    wx.stopPullDownRefresh()
  },
})
 
