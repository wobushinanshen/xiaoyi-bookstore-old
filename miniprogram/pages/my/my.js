// pages/my/my.js
import {OrderModel} from '../../models/OrdelModel.js'

let orderModel = new OrderModel();
var App = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: [],
    defaultImg: '../../images/my/header.png',
    orders: [],
    openId: ""
  },
  /**
   * 生命周期函数--监听页面显示
   */
  //我的订单
  myOrders:function(){
    wx.navigateTo({
      url: '../orders/orders',
    })
  },
  //地址管理
  myAddress:function(){
    wx.navigateTo({
      url: '../address/addressList/addressList',
    })
  },
  //关于我们
  //帮助中心
  //商家入口
  adminPage:function(){
    wx.navigateTo({
      url: '../login/login',
    })
  },
  onLoad: function () {
    this.getOpenid()
    this._init();
  },
  // 初始化
  _init: function () {
    let that = this
    wx.getSetting({
      success:(res)=> {
        if (res.authSetting['scope.userInfo']) {
          // 获取用户信息  
          wx.getUserInfo({
          success: (res) =>{
            let userInfo = res.userInfo
            userInfo.openid = that.data.openId
            that.setData({
            userInfo:userInfo
            })
            console.log('userInfo',that.data.userInfo)
          }
        })
        }
      }
    })
   
  },
  onClick:function(e){
    let active = e.detail.active.toString()
    if(active == '4'){
      wx.showToast({
        title: '该功能暂未开通',
        icon:'none'
      })
    }else{
      wx.navigateTo({
        url:'../orders/orders?active='+active,
        success:res=>{
          console.log('res',res)
        }
      })
    }
  },
  showAddress:function(){
    wx.getSetting({
      success: (res) => {
        //检查登录状态
        if (!res.authSetting['scope.userInfo']) {
          wx.showModal({
            title: '授权提示',
            content: '下单需要在个人中心授权！',
            success(res) {
              wx.switchTab({
                url: '/pages/my/my'
              })
            }
          })
        }else{
          wx.redirectTo({
            url: '../address/addressList/addressList',
          })
        }
      } 
    })
  },
  // 订单页面
  pay: function (event) {
    let id = orderModel.getDataSet(event, 'id')
    wx.navigateTo({
      url: '/pages/order/order?id=' + id
    })
  },
  // 获取用户openid
  getOpenid() {
  let that = this;
  wx.cloud.callFunction({
    name: 'login',
    complete: res => {
      let openId = res.result.openid
      that.setData({
        openId:openId
      })
    }
  })
  
},
  // 用户信息获取
  getuserinfo:function(event){
    console.log('a')
    let userInfo = event.detail.userInfo
    userInfo['openid']=this.data.openId
    this.setData({
      userInfo:userInfo
    })
  },
  aboutUs:function(){
    wx.showToast({
      title: '该功能暂未开通',
      icon:'none'
    })
  },
  help:function(){
    // wx.showToast({
    //   title: '该功能暂未开通',
    //   icon:'none'
    // })
    wx.navigateTo({
      url: '../leave/index/index',
    })
  },
  deleteStorage:function(){
    console.log('deleteStorage')
    wx.showModal({
      title: '提示',
      content: '清除缓存后，购物车中的信息也将会被清空!',
      success (res) {
        if (res.confirm) { 
          wx.clearStorage({
            success(res){
              wx.showToast({
                title: '缓存清除成功',
              })
            },
            fail(res){
              wx.showToast({
                title: '缓存清除失败',
                icon:'none'
              })
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
   
  },
  signIn:function(){
    wx.navigateTo({
      url: '/pages/signIn/signIn',
    })
  },

})
