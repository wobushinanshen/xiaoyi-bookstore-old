// pages/admin/productList/productList.js
import { ProductModel }  from "../../../models/productModel.js"
let Product = new ProductModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        products: [],
        result:[],
        checked:false,
        imgPath:"",
        product:{}

    },
    //1 商品复选框
    onChange(event) {
        this.setData({
          result: event.detail,
        });
        console.log('result',this.data.result)
      },

    //2 全选或全不选
    selectAll(event){
        this.setData({
            checked:event.detail
        })
        let products = this.data.products
        let selected = []
        let noselect = []
        for (const i in products) {
            
            selected.push(products[i]._id)
        }
        if(this.data.checked){
            this.setData({
                result:selected
            })
        }else{
            this.setData({
                result:noselect
            })
        }
    },
   // 删除云端图片
   delCloudImages:function(filePath){
        let filePaths = []
        if(!(filePath instanceof Array)){
             filePaths = [filePath]
        }else{
            filePaths = filePath
        }
        console.log('filepath',filePaths)
        wx.cloud.deleteFile({
            fileList:filePaths,
            success:res=>{
                console.log('删除商品图片成功',res)
            }
        })
    },
    // 删除图片
    delImages:function(productId){
        let that = this
        Product.getProductById(productId,res=>{
            let product_imgs = res.result.data.data.product_imgs
            let detail_imgs =  res.result.data.data.detail_imgs          
            that.delCloudImages(product_imgs)
            that.delCloudImages(detail_imgs)
       })
    },
    //3 删除单个商品
    //********** ? ********
    productDetails: function(e){
        let that = this
        //第一步: 删除图片
        this.delImages(e.detail.productId)
        //第二步: 删除数据库数据
        console.log('id',e.detail.productId)
        Product.delProduct(e.detail.productId,res=>{
            console.log('删除单个成功')
            that.onLoad()
        })
        
    },
    //4  选中多个商品 删除
    _delProducts:function(){
    let products = this.data.result
    let that = this
    if(products.length>0){
        for(var item in products ){
            // 第一步：删除图片
            console.log('item',products[item])
            that.delImages(products[item])
            // 第二步：删除数据
            Product.delProduct(products[item],res=>{
                console.log('选中删除数据成功',res)
                that.onLoad()
            })
         }
    }else{
        wx.showToast({
          title: '请先选择要删除的商品',
          icon:'none'
        })
    }
   
    
    

    },
      _cancle:function(){
        wx.navigateBack({
          delta: 1,
        })
      },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()

    },
    _init:function(){
        Product.getProduct(res=>{
            this.setData({
                products: res.result.data.data
            })
            console.log('res',this.data.result)
        
        })
    },

    productBanner: function (event) {
        let product_id = Product.getDataSet(event, "productid")
        this._navProductDetail(product_id)
        
    },
    // 跳转详情
    _navProductDetail: function (product_id) {
        wx.navigateTo({
        url: '../modifyproduct/modifyproduct?product_id=' + product_id,
        })
    },
   

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})