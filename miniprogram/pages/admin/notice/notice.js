// pages/admin/notice/notice.js
import { NoticeModel } from '../../../models/NoticeModel.js'
let Notice = new NoticeModel()
import Toast from '../../../components/dist/toast/toast'
var app  = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        disabled:true,
        notices:[],
        value: ""

    },
    modifyNotice:function(event){
        console.log('e',event)
        let noticeId = event.currentTarget.dataset.id
        wx.navigateTo({
          url: '../modifyNotice/modifyNotice?noticeId=' +noticeId ,
        })
    },
    _subString:function(array, field, length){
        for(let i=0;i<array.length;i++){
            if(array[i][field].length > length){
                array[i][field] = array[i][field].substr(0,length) + '...'
            }
        }
        return array
    },
    _init:function(){
        Notice.getNotice(res=>{
            console.log('查询成功',res.result.data.data)
            let notices = res.result.data.data
            // notices = this._subString(notices, 'notice', 9)

            this.setData({
                notices:notices,
            })
        })
    },
    submit:function(e){
        let notice = e.detail.notice
        let noticeId = notice._id
        delete notice._id
        let that = this
        notice.update_time = new Date().toISOString()
        notice.create_time = new Date().toISOString()
        console.log('notice',notice)
        Notice.updateNotice(notice, noticeId, res=>{
            console.log('修改公告成功')
            that.onLoad()
        })
    },
    cancel:function(e){
        wx.navigateBack({
          delta: 1,
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },

   
})