// pages/admin/addCategory/addCategory.js
import { CategoryModel } from "../../../models/CategoryModel.js"
import { MyDate } from "../../../models/MyDate.js"

let Category = new CategoryModel()
let Mydate = new MyDate()
Page({
    onChange(event) {
        if(event.detail.length>0){
            this.setData({
                disabled:false,
                categoryName:event.detail
            })
        }else{
            this.setData({
                disabled:true
            })
        }
      },
      _cancle:function(){
        wx.navigateBack({
            delta: 1,
        })
      },
      _addCategory:function(){
        let category = {
            category_name: this.data.categoryName,
            category_type: this.data.categories.length+1,
            create_time: Mydate.getDate(),
            update_time: Mydate.getDate()
        }
        console.log('category',category)
        Category.addCategory(category,res=>{
            wx.showToast({
                title: '添加分类成功',
                duration:1000
              }) 
        })
        wx.navigateTo({
            url: '../index/index',
          })
        
      },

    /**
     * 页面的初始数据
     */
    data: {
        categoryName:"",
        disabled:true,
        categories:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        Category.getCateGory(res=>{
            this.setData({
                categories:res.result.data.data
            })
        })
    },

})