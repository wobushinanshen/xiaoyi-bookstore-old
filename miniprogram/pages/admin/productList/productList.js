// pages/admin/productList/productList.js
import { ProductModel }  from "../../../models/productModel.js"
let Product = new ProductModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        products: []

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()

    },
    _init:function(){
        Product.getProduct(res=>{

            this.setData({
                products: res.result.data.data
            })
        
        })
    },
        // 跳转商品详情
    productDetails: function (e) {
        
        this._navProductDetail(e.detail.productId)
    },
    productBanner: function (event) {
        let product_id = Product.getDataSet(event, "productid")
        this._navProductDetail(product_id)
        
    },
    // 跳转详情
    _navProductDetail: function (product_id) {
        wx.navigateTo({
        url: '../modifyproduct/modifyproduct?product_id=' + product_id,
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})