// pages/admin/index/index.js
import { NoticeModel } from '../../../models/NoticeModel.js'
import Toast from '../../../components/dist/toast/toast'
let Notice = new NoticeModel()
var app = getApp()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        notices:[],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    onShow:function(){
      this._init()
    },
    checkNotice:function(){
        if(this.data.notice === ""){
            this.setData({
                isShow:false
            })
        }else{
            this.setData({
                isShow:true
            })
        }
    },
    _init:function(){
      //公告
      Notice.getNotice(res=>{
        this.setData({
            notices:res.result.data.data,
        })
      })
    },
    closeNotice:function(){
        //判断是否已经开启公告
        if(this.data.isShow){
            let notice = {release:false}
            Notice.updateNotice(notice, res=>{
                Toast.success('公告已关闭')
                wx.navigateTo({
                  url: '../index/index',
                })
            })
        }else{
            Toast.fail('公告尚未发布')
        }
    },
    modifyBanner:function(){

    },
    CheckMsg:function(msg){ 
        let call =  wx.cloud.callFunction({
           name:'ContentCheck',
           data: {
             $url:'MsgCheck',
             msg:msg
           }
         }).then(MsgRes=>{
           if (MsgRes.result.data.errCode == "87014") {
             return Promise.resolve(false)
           }else{
             return Promise.resolve(true)
           }
         }).then(res=>{
            return res
         })
       return call
       },
     /**
      * 
      * @param {*} filePath  图片临时存储地址
      * @param {*} number    第几张图片
      * @param {*} max       可上传的图片最大数量
      */
      ImgCheck:function(filePath,number){
        wx.showLoading({
          title: '图片加载中',
        })
         // 图片转化buffer后，调用云函数
         let that = this
         let call = Promise.resolve('f')
         wx.getFileSystemManager().readFile({
           filePath: filePath,  
           success: res => {
              call = wx.cloud.callFunction({
                name: "ContentCheck",
                data: {
                  $url:'ImgCheck',
                  ImgBuffer: res.data,
                }
              }).then(
                imgRes => { 
                  if (imgRes.result.data.errCode == "87014") {
                    wx.hideLoading({
                      complete: (res) => {},
                    })
                    wx.showToast({
                      title: "图片"+number+"含有违法违规内容",
                      icon: "none"
                    })
                  } else {
                    //图片正常 
                    wx.hideLoading({
                      complete: (res) => {},
                    })
                    if (that.data.productImages.length == 0) {
                      that.setData({
                        productImages: [{url:filePath}],
                      })
                      that.checkInput()
                    } else {   
                      that.setData({
                        productImages: that.data.productImages.concat({url:filePath})
                      })
                   } 
                  } 
                },
                function (error) {
                  wx.hideLoading({
                    complete: (res) => {},
                  })
                  wx.showToast({
                    title: "失败，请重试",
                    icon: "none"
                  })
             })
        } 
       }) 
      },
})