import {CouponModel} from '../../../../models/CouponModel.js'
let Coupon = new CouponModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        radio: '1',
        beginDay: '2016-09-01',
        endDay:'2016-09-01',
        time: '12:01',
        couponName:'',
        subPrice:'',
        sumPrice:'',
        
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        let coupon = {}
        Coupon.getCoupon(coupon, res=>{
            // let coupons = res.result.data.data
            console.log('coupons',res.result.data.data)
        })
        this._getToday()
    },
    _getToday:function(){
        let date = new Date()
        let day = ''
        day += date.getFullYear() + '-'
        day += date.getMonth() + '-'
        day += date.getDate()
        this.setData({
            beginDay:day,
            endDay:day
        })
        console.log('date',day)
    },
    // 单选框
    changeRadio:function(event) {
        this.setData({
          radio: event.detail,
        })
        this.onShow()
    },
    // 输入部分
    inputCouponName:function(e){
        console.log('e',e)
        let couponName = e.detail
        this.setData({couponName})
    },
    inputSubPrice:function(e){
        let subPrice = e.detail
        this.setData({subPrice})
    },
    inputSumPrice:function(e){
        let sumPrice = e.detail
        this.setData({sumPrice})
    },
    // 起始时间
    beginDay:function(e){
        let startDay = e.detail.date
        this.setData({startDay})
    },
    // 结束时间
    endDay:function(e){
        let endDay = e.detail.date
        this.setData({endDay})
    },
    // 校验为空
    _isEmpty:function(str){
        if(str == null || str == ''){
            return true
        }else{
            return false
        }
    },
    // 校验数字
    _isNumber:function(number){
        var reg = /^[0-9]*$/
        if(!reg.test(number)){
            return false
        }else{
            return true
        }
    },
    // 校验起始时间 结束时间
    _checkDay:function(begin,end){
        let beginDay = parseInt(begin.replace(/-/g,'')) 
        let endDay = parseInt(end.replace(/-/g,'')) 
        if(beginDay > endDay){
            return false
        }else{
            return true
        }
    },
    _checkSubmit:function(coupon){

        if(this._isEmpty(coupon.coupon_name)){
            return '优惠券名称不能为空!'
        }
        if(this._isEmpty(coupon.sub_price)){
            return '优惠金额不能为空!'
        }
        if(!this._isNumber(coupon.sub_price)){
            return '优惠金额需为数字!'
        }

        if(coupon.radio == 1){
            if(this._isEmpty(coupon.sum_price)){
                return '满足金额不能为空!'
            }
            if(!this._isNumber(coupon.sum_price)){
                return '满足金额需为数字!'
            }
        }
        if(!this._checkDay(this.data.beginDay,this.data.endDay)){
            return '优惠时间不合法!'
        }
        return ''
    },
    cancel:function(){
        wx.navigateBack({
          delta: 1,
        })
    },
    submit:function(e){
        let couponV = e.detail.value
        console.log('submit',e.detail.value)

        let message = this._checkSubmit(couponV)
        if(message.length > 0 ){
            wx.showToast({
              title: message,
              icon:'none'
            })
        }else{
            let coupon = {
                coupon_name : couponV.coupon_name,
                begin_time : this.data.beginDay,
                end_time : this.data.endDay,
                type : couponV.radio ,
                sub_price : parseInt(couponV.sub_price) 
            }
            if(couponV.radio == 1){
                coupon.sum_price  = parseInt(couponV.sum_price) 
            }
            Coupon.addCoupon(coupon, res=>{
                console.log('成功',res)
            })
           
        }
    }


   
})