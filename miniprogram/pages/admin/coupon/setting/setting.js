import {ShopModel} from '../../../../models/ShopModel.js'

let Shop = new ShopModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        shop:{},
        count:0,
        money:0,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        this._getShop()
       
    },
    _getShop:function(){
        let shop = {}
        Shop.getShop(shop, res=>{
            let result = res.result.data.data
            if(result.length > 0){
                this.setData({
                    shop:result[0],
                    money:result[0].coupon_money,
                    count:result[0].coupon_count
                })
            }
           
            console.log('result',result)
        })
    },
    _inputCount:function(e){
        let count = e.detail
        this.setData({count})
        console.log(this._isEmpty(this.data.count))
    },
    _inputMoney:function(e){
        let money = e.detail
        this.setData({money})
        console.log(this._isEmpty(this.data.money))

    },
     // 校验数字
     _isNumber:function(number){
        var reg = /^[1-9][0-9]*$/
        if(!reg.test(number)){
            return false
        }else{
            return true
        }
    },
    _isEmpty:function(str){
        if(str == '' || str == null){
            return true
        }else{
            return false
        }
    },
    _checkSubmit:function(){
        if(this._isEmpty(this.data.count)){
            return '优惠券数量不能为空!'
        }
        if(!this._isNumber(this.data.count)){
            return '优惠券数量应为正整数!'
        }
        if(this._isEmpty(this.data.money)){
            return '优惠券金额不能为空!'
        }
        if(!this._isNumber(this.data.count)){
            return '优惠券金额应为正整数!'
        }
        return ''
    },
    cancel:function(){
        wx.navigateBack({
          delta: 1,
        })
    },
    confirm:function(){
        let that = this
        let message = that._checkSubmit()
        if(message !== ''){
            wx.showToast({
              title: message,
              icon:'none'
            })
        }else{
            let shop = {
                coupon_count:parseInt(that.data.count),
                coupon_money:parseInt(that.data.money)
            }
            Shop.updateShop(shop, that.data.shop._id, res=>{
                console.log('update success',res)
                that.cancel()

              
        })
        }
    },

  
})