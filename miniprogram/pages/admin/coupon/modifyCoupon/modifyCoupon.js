import {CouponModel} from '../../../../models/CouponModel.js'
const base64  = require('../../../../utils/base64.js');
const aes  = require('../../../../utils/aes.js');
let Coupon = new CouponModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        radio: '1',
        beginDay: '2016-09-01',
        endDay:'2016-09-01',
        time: '12:01',
        couponName:'',
        subPrice:'',
        sumPrice:'',
        coupon:{},
        couponId:'',
        count:0,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let couponId = options.couponId
        this._getCouponById(couponId)


        // this._init()
    },
    _getCouponById:function(couponId){
        let coupon = {
            _id:couponId
        }
        Coupon.getCoupon(coupon, res=>{
            let coupon = res.result.data.data
            if(coupon.length > 0){
                this.setData({
                    couponId:coupon[0]._id,
                    radio:coupon[0].type,
                    beginDay:coupon[0].begin_time,
                    endDay:coupon[0].end_time,
                    couponName:coupon[0].coupon_name,
                    subPrice:coupon[0].sub_price,
                    sumPrice:coupon[0].sum_price
                })
            }
            console.log('coupons',coupon)
        })
    },
    _init:function(){
        let coupon = {}
        Coupon.getCoupon(coupon, res=>{
            // let coupons = res.result.data.data
            console.log('coupons',res.result.data.data)
        })
        this._getToday()
    },
    // 单选框
    changeRadio:function(event) {
        this.setData({
          radio: event.detail,
        })
        this.onShow()
    },
    // 输入部分
    inputCouponName:function(e){
        let couponName = e.detail
        this.setData({couponName})
    },
    inputSubPrice:function(e){
        let subPrice = e.detail
        this.setData({subPrice})
    },
    inputSumPrice:function(e){
        let sumPrice = e.detail
        this.setData({sumPrice})
    },
    inputCount:function(e){
        let count = e.detail.value
        this.setData({count})
        console.log('e',count)
    },
    // 起始时间
    beginDay:function(e){
        let startDay = e.detail.date
        this.setData({startDay})
    },
    // 结束时间
    endDay:function(e){
        let endDay = e.detail.date
        this.setData({endDay})
    },
    // 校验为空
    _isEmpty:function(str){
        if(str == null || str == ''){
            return true
        }else{
            return false
        }
    },
    // 校验数字
    _isNumber:function(number){
        var reg = /^[0-9]*$/
        if(!reg.test(number)){
            return false
        }else{
            return true
        }
    },
    // 校验起始时间 结束时间
    _checkDay:function(begin,end){
        let beginDay = parseInt(begin.replace(/-/g,'')) 
        let endDay = parseInt(end.replace(/-/g,'')) 
        if(beginDay > endDay){
            return false
        }else{
            return true
        }
    },
    _checkSubmit:function(coupon){

        if(this._isEmpty(coupon.coupon_name)){
            return '优惠券名称不能为空!'
        }
        if(this._isEmpty(coupon.sub_price)){
            return '优惠金额不能为空!'
        }
        if(!this._isNumber(coupon.sub_price)){
            return '优惠金额需为数字!'
        }

        if(coupon.radio == 1){
            if(this._isEmpty(coupon.sum_price)){
                return '满足金额不能为空!'
            }
            if(!this._isNumber(coupon.sum_price)){
                return '满足金额需为数字!'
            }
        }
        if(!this._checkDay(this.data.beginDay,this.data.endDay)){
            return '优惠时间不合法!'
        }
        return ''
    },
    
    submit:function(e){
        let couponV = e.detail.value
        console.log('submit',e.detail.value)

        let message = this._checkSubmit(couponV)
        if(message.length > 0 ){
            wx.showToast({
              title: message,
              icon:'none'
            })
        }else{
            let coupon = {
                coupon_name : couponV.coupon_name,
                begin_time : this.data.beginDay,
                end_time : this.data.endDay,
                type : parseInt(couponV.radio) ,
                sub_price : parseInt(couponV.sub_price) 
            }
            if(couponV.radio == 1){
                coupon.sum_price  = parseInt(couponV.sum_price) 
            }
            Coupon.addCoupon(coupon, res=>{
                console.log('成功',res)
            })
           
        }
    },
    // 修改数据库优惠券数据
    _updateCouponData:function(couponV){
        let coupon = {
            coupon_name : couponV.coupon_name,
            begin_time : this.data.beginDay,
            end_time : this.data.endDay,
            type : parseInt(couponV.radio) ,
            sub_price : parseInt(couponV.sub_price) 
        }
        if(couponV.radio == 1){
            coupon.sum_price  = parseInt(couponV.sum_price) 
        }
        Coupon.updateCoupon(coupon, this.data.couponId, res=>{
            console.log('更新数据成功',res)
            this.cancel()
            // this.onShow()
        })
    },
    // 删除数据库优惠券数据
    _delCouponData:function(couponId){
        Coupon.delCoupon(couponId, res=>{
            console.log('删除成功',res)
            this.cancel()
        })
    },
    // 1、返回
    cancel:function(){
        wx.navigateBack({
          delta: 1,
        })
    },
    // 2、修改
    modifyCoupon:function(e){
        let that = this
        let couponV = e.detail.value
        let message = this._checkSubmit(couponV)
        if(message.length > 0 ){
            wx.showToast({
              title: message,
              icon:'none'
            })
        }else{
            wx.showModal({
                title: '提示',
                content: '确定修改优惠券?',
                success (res) {
                  if (res.confirm) {
                    that._updateCouponData(couponV)

                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
        }
       
    },
    // 3、删除
    delCoupon:function(){
        let that = this 
        wx.showModal({
            title: '提示',
            content: '确定删除优惠券?',
            success (res) {
              if (res.confirm) {
                that._delCouponData(that.data.couponId)
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
    },
    // 4、生成兑换码
    createCode:function(){
        let coupon = {
            _id:this.data.couponId,
            count:this.data.count
        }
        console.log(JSON.stringify(coupon))
        let code = aes.aesEncrypt(JSON.stringify(coupon))
        console.log('encode',code)
        let encode = aes.aesDecrypt(code)
        console.log('decode',encode)

    }


   
})