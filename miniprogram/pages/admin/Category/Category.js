// pages/admin/Category/Category.js
import { CategoryModel } from "../../../models/CategoryModel.js"
import { MyDate } from "../../../models/MyDate.js"
import Notify from '../../../components/dist/notify/notify';
import Toast from '../../../components/dist/toast/toast';

let Category = new CategoryModel()
let Mydate = new MyDate()
Page({
   
    // 1 删除商品分类
    categoryDetails:function(e){
        Category.delCategory(e.detail.categoryId,res=>{
            console.log('删除商品分类成功',res)
        })
        Toast.success('删除成功');
        wx.redirectTo({
          url: '../Category/Category',
        })

    },
    //2 弹窗相关方法

    //显示弹窗
    _addCategory:function(event){
        this.setData({
            show:true
        })
    },
    //点击确定 将输入数据添加到数据库
    confirm:function(e){
        let category = {
            category_name: this.data.input,
            category_type: this.data.categories.length+1,
            create_time: Mydate.getDate(),
            update_time: Mydate.getDate()
        }
        console.log('category',category)
        Category.addCategory(category,res=>{
            console.log('添加成功',res)
        })
        Toast.success('添加成功');
        wx.redirectTo({
            url: '../Category/Category',
          })
      },
      _add:function(){
          this.setData({
            show:true
          })
      },
      onChange(event) {
        if(event.detail.length>0){
          this.setData({
            showConfirm:true
          })
        }else{
          this.setData({
            showConfirm:false
          })
        }
        this.setData({
          input:event.detail
        })
      },
      onClose() {
        this.setData({ close: false });
      },

    /**
     * 页面的初始数据
     */
    data: {
        categories:[],
        show:false,
        input:"",
        showConfirm:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        Category.getCateGory(res=>{
            this.setData({
                categories:res.result.data.data
            })
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})