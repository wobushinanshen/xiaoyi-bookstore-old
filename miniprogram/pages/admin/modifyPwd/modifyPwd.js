import { AdminModel } from "../../../models/AdminModel.js"
let Admin = new AdminModel()
import Toast from '../../../components/dist/toast/toast';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        oldPassword : '',
        newPassword : '',
        confirmPassword : '',
        oldPwdError : '',
        newPwdError : '',
        confirmPwdError : '',
        disabled : true ,
        admin : {}

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        Admin.getAdmin(res =>{
            let admin = res.result.data.data[0]
            this.setData({admin})
        } )
    },
    //  旧密码
    inputOldPwd:function(e){
        let oldPassword = e.detail
        if(oldPassword){
            this.setData({
                oldPwdError : '',
                oldPassword : oldPassword
            })
        }else{
            this.setData({
                oldPwdError : '旧密码不能为空'
            })
        }
        this.checkInput()
    },
    //新密码    
    inputNewPwd:function(e){
        let newPassword = e.detail
        if(newPassword){
            this.setData({
                newPwdError : '',
                newPassword : newPassword
            })
        }else{
            this.setData({
                newPwdError : '新密码不能为空'
            })
        }
        this.checkInput()
    },
    // 确认密码
    inputConfirmPwd:function(e){
        let confirmPassword = e.detail
        if(confirmPassword){
            this.setData({
                confirmPwdError : '',
                confirmPassword : confirmPassword
            })
        }else{
            this.setData({
                confirmPwdError : '确认密码不能为空'
            })
        }
        this.checkInput()
    },
    // 输入不为空
    checkInput:function(){
        if(this.data.oldPassword.length > 0 && this.data.newPassword.length > 0 && this.data.confirmPassword.length > 0){
            this.setData({
                disabled : false
            })
        }else{
            this.setData({
                disabled : true
            })
        }
    },
    modifyPwd:function(){
        if(this.data.confirmPassword !== this.data.newPassword){
            Toast.fail('两次密码输入不一致');
            return
        }
        if(this.data.admin.password !== this.data.oldPassword){
            Toast.fail('旧密码输入错误');
            return
        }
        if(this.data.admin.password == this.data.newPassword){
            Toast.fail('新密码不能与旧密码相同');
            return
        }
        
        let admin = {
            password : this.data.newPassword
        }
        Admin.updateAdmin(admin, this.data.admin._id, res=>{
            console.log('update',res)
            Toast.success('密码修改成功')
            wx.navigateTo({
              url: '../../login/login',
            })
        })
        
    },
    
})