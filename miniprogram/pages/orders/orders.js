// pages/my/my.js
import {OrderModel} from '../../models/OrdelModel.js'
let orderModel = new OrderModel();
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active:0,
    orders: [],
    allOrder:false,
    pendingOrder:[],
    pending:false,
    paidOrder:[],
    paid:false,
    completedOrder:[],
    completed:false,
    openId: "",
    order:{}

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onLoad: function (options) {

    if(options.active){
      this.setData({
        active:parseInt(options.active) 
      })
    }
    //页面传递参数默认为字符串， 如果需要赋值需要注意数据类型转换
    let openId = app.globalData.openId
    this.setData({
      openId:openId
    })
   
    this._init();
    
  },
  // 初始化
  _init: function () {
    let that = this
    let openId = this.data.openId
  
    orderModel.getOrderList(openId, res => {
      console.log("openID",openId)
      console.log('getList',res)
    that.setData({
     orders: res.result.data.data
    })
    console.log('order',this.data.orders)
    that._classifyOrders(that.data.orders)
    that._isShow(that)
    })
  },
  delOrder:function(e){
    let id =  e.detail.orderId
    console.log('id',id)
    orderModel.delOrder(id,res=>{
        console.log('删除订单成功',res)
    })
    wx.navigateTo({
        url: '../orders/orders',
      })
  },
  orderDetails:function(e){
    let id =  e.detail.orderId
    
    let order = {}
    orderModel.getOrderById(id, res=>{
      //获取订单详情
     order = res.result.data.data
    //  order.order_status = 2
     console.log('id',order)
    
      switch(order.order_status){
        case 0 :
          break;
        case 1 :
          {
            delete order._id
            order.order_status = 2
            orderModel.updateOrder(id,order,res=>{
               console.log('更新成功')
               wx.navigateTo({
                 url: '../orders/orders',
               })
             })
          }
        case 2 :
          break
      }
    
    })
  },

  _isShow:function(that){
    if(that.data.orders.length>0){
        that.setData({
          allOrder:false//不显示图标
        })
    }else{
        that.setData({
          allOrder:true//显示图标
        })
    }

    if(that.data.pendingOrder.length>0){
        that.setData({
            pending:false//不显示图标
        })
    }else{
        that.setData({
            pending:true//显示图标
        })
    }

    if(that.data.paidOrder.length>0){
        that.setData({
            paid:false//不显示图标
        })
    }else{
        that.setData({
            paid:true//显示图标
        })
    }

    if(that.data.completedOrder.length>0){
        that.setData({
            completed:false//不显示图标
        })
    }else{
        that.setData({
            completed:true//显示图标
        })
    }
    
  },
  
  _classifyOrders:function(orders){
    let pendingOrder = []
    let paidOrder = []
    let completedOrder = []
    for(var i in orders){
      switch(orders[i].order_status){
        case 0:
          pendingOrder.push(orders[i])
          break;
        case 1:
          paidOrder.push(orders[i])
          break;
        case 2:
          completedOrder.push(orders[i])
          break;
      }
    }
    this.setData({
      pendingOrder: pendingOrder,
      paidOrder: paidOrder,
      completedOrder: completedOrder
    })
  },
    // 订单页面
    pay: function (event) {
        let id = orderModel.getDataSet(event, 'id')
        wx.navigateTo({
        url: '/pages/order/order?id=' + id
        })
    },
    

})
