import { SignInModel } from '../../models/SignInModel.js'
import { CustomerModel } from '../../models/CustomerModel.js'
let SignIn = new SignInModel()
let Customer = new CustomerModel()
let App = getApp()
const date = new Date()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        days_color:[],
        calendar:{},
        disabled:false,
        openId:'',
        customer:{},
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        this._getCalendar()
        this._getCustomer()
    },
    _getCalendar:function(){
        let openId = this._getOpenId()
        let calendar = {
            year:parseInt(date.getFullYear()),
            month:parseInt(date.getMonth()) + 1,
            openid:openId
        }
        console.log('ca',calendar)
        SignIn.getCalendar(calendar,res=>{
            let calendar = res.result.data.data
            console.log('cal',calendar)
            if(calendar.length>0){
                let days_color = this._formatDays(calendar[0].days)
                // 判断今日是否签到
                let disabled = this._judgeToday(calendar[0].days)
                this.setData({
                    calendar:calendar[0],
                    days_color:days_color,
                    disabled:disabled
                })
            }
        })
    },
    _getOpenId:function(){
        let openId = App.globalData.openId
        this.setData({openId})
        console.log('tsts',openId)
        return openId
      },
    // 格式化签到日期
    _formatDays:function(days){
        let days_color = []
        for(let i=0;i<days.length;i++){
            let day = {
                month:'current',
                day:days[i],
                color:'white',
                background:'red'
            }
            days_color.push(day)
        }
        return days_color
    },
    // 判断今日是否已经签到了
    _judgeToday:function(days){
        let date = new Date()
        for(let i=0;i<days.length;i++){
            if(days[i] == parseInt(date.getDate())){
               return true
            }
        }
        return false
    },
    // 点击签到
    signIn:function(){
        let calendar = this.data.calendar
        let date = new Date()
        // 如果本月还未签到 则 新增
        if(JSON.stringify(calendar) == '{}' ){
            calendar.year = parseInt(date.getFullYear())
            calendar.month = parseInt(date.getMonth()) + 1
            calendar.days = [parseInt(date.getDate())]
            calendar.openid = this.data.openId
            SignIn.addCalendar(calendar, res=>{
                wx.showToast({
                  title: '签到成功',
                })
                console.log('新增成功',res)
                this._updateCustomer(2)
                this.onLoad()
            })
        }else{
        // 本月已签到 则 更新
            calendar.days.push(parseInt(date.getDate()))
            let newCalendar = {
                days: calendar.days
            }
            console.log('new',parseInt(date.getDate()),calendar)
            let calendarId = calendar._id
            delete calendar._id
            SignIn.updateCalendar(newCalendar,calendarId, res=>{
                console.log('更新成功',res)
                wx.showToast({
                    title: '签到成功',
                  })
                this._updateCustomer(2)
                this.onLoad()
            })
        }
       
    },

   
    prev: function (event) {
        console.log(event.detail);
    },

    next: function (event) {
        console.log(event.detail);
    },
    _getCustomer:function(){
        let customer = {}
        Customer.getCustomer(customer, res=>{
            let customer = res.result.data.data
            if(customer.length>0){
                this.setData({
                    customer:customer[0]
                })
            }
           
            console.log('customer',customer)
        })
    },
    // 更新用户积分
    _updateCustomer:function(number){
        let customer = {
            consumption:this.data.customer.consumption + parseInt(number)
        }
        Customer.updateCustomer(customer, this.data.customer._id, res=>{

            console.log('更新积分成功',res)
            console.log(customer,this.data.customer._id)
        })
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})