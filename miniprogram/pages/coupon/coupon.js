import {CouponModel} from '../../models/CouponModel.js'
let DateUtil = require('../../utils/dateUtil.js')
let Coupon = new CouponModel()
const  Payment  = require('../../utils/payment.js')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        radio: '1',
        beginDay: '2016-09-01',
        endDay:'2016-09-01',
        time: '12:01',
        couponName:'',
        subPrice:'',
        sumPrice:'',
        coupons:[],
        date:''
        
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
        let date = DateUtil.getDate()
        console.log('date',DateUtil.formatDate(date))
        this.setData({date})
    },
    _init:function(){
        this._getCoupons()
        this._getToday()
    },
    test: async function(){
        let order = {
            body : '月度会员',
            money : 1 * 100,
        }
        let params = {
            url:'unifiedOrder',
            data:order
        }
        
        let cloud = await Payment.unifiedOrder(params)
        console.log('cloud',cloud)
        let pay = await Payment.Pay(cloud.result.data.payment)
        console.log('pay',pay)
    },
    _getCoupons:function(){
        Coupon.getCoupon({},res=>{
            let coupons = res.result.data.data
            if(coupons.length > 0){
                this.setData({coupons})
            }
            console.log('res',coupons)
        })
    },  
    _getToday:function(){
        let date = new Date()
        let day = ''
        day += date.getFullYear() + '-'
        day += date.getMonth() + '-'
        day += date.getDate()
        this.setData({
            beginDay:day,
            endDay:day
        })
        console.log('date',day)
    },
   showDetail:function(e){
       let couponId = e.detail.couponId
        console.log('e',couponId)
        wx.navigateTo({
          url: '../modifyCoupon/modifyCoupon?couponId=' + couponId,
        })
   },
   addCoupons:function(){
        let coupon = {
            couponId:b1a52c595fc06f0000808e2e330dc7d4,
            count:6,
            
        }
   },


   
})