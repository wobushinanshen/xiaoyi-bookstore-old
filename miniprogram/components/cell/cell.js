// components/cell/cell.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        onClick:function(e){
            console.log('e',e.currentTarget.dataset.active)
            this.triggerEvent("onClick",{active:e.currentTarget.dataset.active},{})
        }

    }
})
