// components/x-select-day/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        label:String,
        date:String,
        satrt:String,
        end:String
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
      selectDate: function(e) {
            this.setData({
              date: e.detail.value
            })
            this.triggerEvent('selectDate',{date:this.data.date},{})
          },
       
    }
})
