// components/category-column/category-column.js
let categoryBehavior = require('../behaviors/category-behavior.js')
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },
    behaviors:[categoryBehavior]
    ,

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})
