// components/sku/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        product:Object

    },

    /**
     * 组件的初始数据
     */
    data: {
        show:true
    },
    
    /**
     * 组件的方法列表
     */
    methods: {
        onClose() {
            this.setData({ show: false });
          },


    }
})
