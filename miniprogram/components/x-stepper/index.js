// components/x-stepper/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        count:0
    },

    /**
     * 组件的方法列表
     */
    methods: {
        addCount:function(){
            this.setData({
                count:this.data.count+1
            })
            this.triggerEvent(
                'addCount',{count:this.data.count},{}
            )
        },
        subCount:function(){
            this.setData({
                count:this.data.count-1
            })
            this.triggerEvent(
                'subCount',{count:this.data.count},{}
            )
        },

    }
})
