// components/book-cart/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
      product:Object
    },

    /**
     * 组件的初始数据
     */
    data: {
      checked: true,
      },
     
   

    /**
     * 组件的方法列表
     */
    methods: {
        onLoad:function(){
          this.setData({
            checked:this.data.product.selectStatus
          }) 
        },
         allSelect:function(){
          this.setData({
            checked:true
          })
        },
        noSelect:function(){
          this.setData({
            checked:false
          })

        },
        ChangeRadio(event) {
            this.setData({
              checked: event.detail,
            });
            this.triggerEvent("ChangeRadio",{productId:this.data.product._id,status:this.data.checked},{})
        },
        ChangeStepper(event) {
          this.triggerEvent("changeCount",{productId:this.data.product._id,count:event.detail},{})
          },
        showDetail(event) {
            this.triggerEvent("showDetail",{productId:this.data.product._id},{})
          },
            
    }
})
