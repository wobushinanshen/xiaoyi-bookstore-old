// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { SHOP } = require('../config/tableConfig.js')

const { SHOPFIELD } = require('../fields/shopField.js')

const addShop = (shop)=>{
    return model.add(SHOP, shop)
}
const getShop = (shop) => {
    return model.query(SHOP, SHOPFIELD, shop )
}
const updateShop = (shop, shopId) => {
  return model.update(SHOP, shop, shopId)
}
  module.exports = {
    getShop,
    updateShop,
    addShop,

  }