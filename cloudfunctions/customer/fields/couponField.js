module.exports = {
    COUPONFIELD:{
        coupon_name:true,
        start_time:true,
        end_time:true,
        sub_price:true,
        sum_price:true,
        type:true,
        count:true,
    }
}