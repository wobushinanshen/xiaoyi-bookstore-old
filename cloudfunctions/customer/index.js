// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router');
// 返回工具类
const returnUtil = require('utils/ReturnUtil.js')
// 商品信息业务层

const Customer = require('service/customerService.js')
const Coupon = require('service/couponService.js')
const Shop = require('service/shopService.js')
const SignIn = require('service/signInService.js')
cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: 'ibookshop-1wxto'
  
})
const IMAGEPREFIX = "cloud://ibookshop-1wxto.6962-ibookshop-1wxto-1303227463"

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const app = new TcbRouter({ event });
  // app.use 表示该中间件会适用于所有的路由
  app.use(async (ctx, next) => {
    ctx.data = {};
    await next(); // 执行下一中间件
  });
   /***************************    日历  *****************************************/    
  
   //  1、获取用户已签到日期
   app.router('getCalendar',async (ctx,next) => {
    let calendar = event.data.calendar
    ctx.data = await SignIn.getCalendar(calendar)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  // 2、更新本月签到信息
  app.router('updateCalendar',async (ctx,next) => {
    let calendar = event.data.calendar
    let calendarId = event.data.calendarId
    ctx.data = await SignIn.updateCalendar(calendar, calendarId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  // 3、新增本月签到数据
  app.router('addCalendar',async (ctx,next) => {
    let calendar = event.data.calendar
    ctx.data = await SignIn.addCalendar(calendar)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  /***************************    日历  *****************************************/    
 
  // 1、添加用户数据
  app.router('addCustomer',async (ctx,next) => {
    let customer = event.data.customer
    ctx.data = await Customer.addCustomer(customer)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 2、获取用户数据
  app.router('getCustomer',async (ctx,next) => {
    let customer = event.data.customer
    ctx.data = await Customer.getCustomer(customer)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 3、更新用户数据
  app.router('updateCustomer',async (ctx,next) => {
    let customer = event.data.customer
    let customerId = event.data.customerId
    ctx.data = await Customer.updateCustomer(customer, customerId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 4、删除用户数据
  app.router('delCustomer',async (ctx,next) => {
    let customerId = event.data.customerId
    ctx.data = await Customer.delCustomer(customerId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  /***************************    优惠券  *****************************************/    
 
  // 1、新建优惠券
  app.router('addCoupon',async (ctx,next) => {
    let coupon = event.data.coupon
    ctx.data = await Coupon.addCoupon(coupon)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 2、获取优惠券
  app.router('getCoupon',async (ctx,next) => {
    let coupon = event.data.coupon
    ctx.data = await Coupon.getCoupon(coupon)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 3、更新优惠券
  app.router('updateCoupon',async (ctx,next) => {
    let coupon = event.data.coupon
    let couponId = event.data.couponId
    ctx.data = await Coupon.updateCoupon(coupon, couponId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 4、删除优惠券
  app.router('delCoupon',async (ctx,next) => {
    let couponId = event.data.couponId
    ctx.data = await Coupon.delCoupon(couponId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  /***************************    shop  *****************************************/

   // 1、获取shop
   app.router('getShop',async (ctx,next) => {
    let shop = event.data.shop
    ctx.data = await Shop.getShop(shop)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  app.router('updateShop',async (ctx,next) => {
    let shop = event.data.shop
    let shopId = event.data.shopId
    ctx.data = await Shop.updateShop(shop, shopId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })



  return app.serve();
}
