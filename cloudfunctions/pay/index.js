const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router')
const ReturnUtil = require('utils/ReturnUtil.js')
const uuidUtil = require('utils/uuidUtil.js')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

exports.main = async (event, context) => {   
  const app = new TcbRouter({ event }) 

  // app.use 表示该中间件会适用于所有的路由
  app.use(async (ctx, next) => {
      ctx.data = {};
      await next(); // 执行下一中间件
  });
  //统一下单
  app.use('unifiedOrder', async(ctx, next) => {
    const res = await cloud.cloudPay.unifiedOrder({
      "body" : event.data.body,
      "outTradeNo" : uuidUtil.uuid(32,32),
      "spbillCreateIp" : "127.0.0.1",
      "subMchId" : "1602886525",//这里要注意：虽然key是子商户id，实际上就是普通商户id
      "totalFee" : parseInt(event.data.money),//第二个坑：注意必须是数字，如果不是数字，则会报错unifiedOrder:fail wx api error: -202
      "envId": "chicken-shop",//这里是回调函数所属的的云环境id
      "functionName": "payCallBack",//这个是回调函数名
      "nonceStr": uuidUtil.uuid(32,32),//第三个坑：官方文档中相关云函数代码没有nonceStr和tradeType，测试的时候会报nonceStr不存在的错，翻看文档才发现这个是必填项，直接粘过来以后还需要加上这两个参数
      "tradeType":"JSAPI",
    })
    ctx.data = res
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })

  // 订单查询
  app.use('queryOrder', async(ctx, next) => {
    const res = await cloud.cloudPay.queryOrder({
      'subMchId':'1603763918',
      'outTradeNo':event.data.order.orderId,
      "nonceStr": event.data.order.nonceStr
    })
    ctx.data = res
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })

   // 关闭订单
   app.use('closeOrder', async(ctx, next) => {
    const res = await cloud.cloudPay.closeOrder({
      'subMchId':'1603763918',
      'outTradeNo':event.data.order.orderId,
      "nonceStr": event.data.order.nonceStr
    })
    ctx.data = res
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })

   // 申请退款
   app.use('refund', async(ctx, next) => {
    const res = await cloud.cloudPay.refund({
      'subMchId':'1603763918',
      'outTradeNo':event.data.order.orderId,
      "nonceStr": event.data.order.nonceStr,
      'outRefundNo': event.data.order.outRefundNo,//商户退款单号
      'totalFee': event.data.order.totalFee,//订单金额
      'refundFee': event.data.order.refundFee//申请退款金额
    })
    ctx.data = res
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })

   // 查询退款
   app.use('QueryRefund', async(ctx, next) => {
    const res = await cloud.cloudPay.closeOrder({
      'subMchId':'1603763918',
      'outTradeNo':event.data.order.orderId,
      // "nonceStr": event.data.order.nonceStr,
      'outRefundNo': event.data.order.outRefundNo,//商户退款单号
     
    })
    ctx.data = res
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })








return app.serve();
const res = await cloud.cloudPay.unifiedOrder({
    "body" : event.body,
    "outTradeNo" : event.orderId,
    "spbillCreateIp" : "127.0.0.1",
    "subMchId" : "1602886525",//这里要注意：虽然key是子商户id，实际上就是普通商户id
    "totalFee" : parseInt(event.money),//第二个坑：注意必须是数字，如果不是数字，则会报错unifiedOrder:fail wx api error: -202
    "envId": "ibookshop-1wxto",//这里是回调函数所属的的云环境id
    "functionName": "payCallBack",//这个是回调函数名
    // "nonceStr":'H7I6KB8QUA183DE5GUHI1PQLAKH7J92R',//第三个坑：官方文档中相关云函数代码没有nonceStr和tradeType，测试的时候会报nonceStr不存在的错，翻看文档才发现这个是必填项，直接粘过来以后还需要加上这两个参数
    // "tradeType":"JSAPI",
   
  })
  return res
}