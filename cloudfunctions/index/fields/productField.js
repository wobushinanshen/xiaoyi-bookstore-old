// product 指定返回结果中记录需返回的字段
module.exports = {
  PRODUCTFIELD: {
    product_name: true,
    product_imgs: true,
    detail_imgs:true,
    product_price: true,
    product_sell_price: true,
    product_stock: true,
    product_description:true,
    product_tag:true,
    category_type:true,
    product_theme:true,
    sold_count:true,
    // create_time:false,
    // update_time:false,
    



  }
}
