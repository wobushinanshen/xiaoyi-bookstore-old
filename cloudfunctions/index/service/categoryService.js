// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { PRODUCT_CATEGORY } = require('../config/tableConfig.js')
// 返回字段处理
const { PRODUCT_CATEGORY_FIELD } = require('../fields/productCategoryField.js')

const addCategory = (category)=>{
    return model.add(PRODUCT_CATEGORY,category)
}
const delCategory = (categoryId)=>{
    return model.remove(PRODUCT_CATEGORY,categoryId)
}
module.exports = {
    addCategory,
    delCategory,
}