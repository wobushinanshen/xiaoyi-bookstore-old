// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { PRINTER } = require('../config/tableConfig.js')
// 返回字段处理
const { PRINTERFIELD } = require('../fields/printerField.js')
/**
 * 获取首页轮播
 * @return 
 */
const getPrinter = () => {
  return model.query(PRINTER, PRINTERFIELD, {} )
}

const updatePrinter = (printer, printerId) => {
    return model.update(PRINTER, printer, printerId)
}

// 导出
module.exports = {
    getPrinter,
    updatePrinter,

}
