const model = require('../models/BaseModel.js')
const { PRODUCTFIELD } = require('../fields/productField.js')

// 在原来的上面增加 PRODUCT_CATEGORY
const { PRODUCT ,PRODUCT_CATEGORY } = require('../config/tableConfig.js')
// 新增分类字段过滤
const { PRODUCT_CATEGORY_FIELD } = require('../fields/productCategoryField.js')

/**
 * 获取单个商品
 * @param product_id 条件
 * @return 
 */
const getProductById = (product_id) => {
  return model.findById(PRODUCT, PRODUCTFIELD, product_id)
}
/**
 * 获取商品主题
 * @param product_theme 条件
 * @return 
 */
const getThemeProduct = (product_theme) => {
  let options = {product_theme:product_theme}
  return  model.query(PRODUCT, PRODUCTFIELD, options)
}

/**
 * 获取商品分类
 * @return 
 */
const getCategoryMenu = () =>{
  return model.query(PRODUCT_CATEGORY,PRODUCT_CATEGORY_FIELD)
}

/**
 * 根据商品分类获取商品
 * @param {*} options 
 */
const getCategoryProduct = (options) => {
  options.product_status = 0 
  return model.query(PRODUCT, PRODUCTFIELD, options)
}



/**
 * 获取商品
 * @param options 条件
 * @param page    
 * @param size
 * @return 
 */
const getProduct = (options , page = 0, size = 10 , order = {} ) => {
  // 查询条件
  options.product_status = 0
  // 排序条件 根据需要调正优化
  order.name = 'create_time'
  order.orderBy= 'asc'
  return model.query(PRODUCT, PRODUCTFIELD, options, page,size,order)
}

/**
 * 新增商品
 */
const addProduct=(product)=>{
  return model.add(PRODUCT, product)
}
/**
 *  更新商品
 * @param {*} product 
 * @param {*} productId 
 */
const updateProduct = (product,productId)=> {
  return model.update(PRODUCT, product,productId)
}

/**
 * 删除商品
 * @param {r} productId 
 */
const delProduct = (productId)=> {
  return model.remove(PRODUCT,productId)
}

const regQuery = (regexp, columns) => {
  return model.regexpQuery(regexp, PRODUCT, columns[0], columns[1])
}




module.exports = {
  getProduct,
  getProductById,
  getCategoryMenu,
  getCategoryProduct,
  getThemeProduct,
  addProduct,
  updateProduct,
  delProduct,
  regQuery
}
