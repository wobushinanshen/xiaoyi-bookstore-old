// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { ADDRESS } = require('../config/tableConfig.js')
// 返回字段处理
const { ADDRESSFIELD } = require('../fields/addressField.js')


const getAddressById = (addressId) => {
  return model.findById(ADDRESS, ADDRESSFIELD, addressId)
}
const getAddressList = (openid)=>{
  // 查询需要前天显示的轮播
  // 传递参数   对应的BaseModel的方法名称
  let options = {buyer_openid:openid}
  return model.query(ADDRESS, ADDRESSFIELD, options )
}
/**
 * 重置地址
 */
const resetAddress = (openid) =>{
    let options = {
      buyer_openid:openid,
      default:true
    }
    let params = {default:false}
    model.updateCollection(ADDRESS, params, options)
}

const resetAddress2 = (openid) =>{
  let options = {
    buyer_openid:openid,
    default:true
  }
  let params = {default:false}
  model.updateCollection(ADDRESS, params, options)
}
/**
 * 设置默认地址
 * @param {*} addressId 
 */
const setDefaultAddress = (addressId) => {
    model.update(ADDRESS,ADDRESSFIELD,addressId)
}
/**
 * 增加收货地址
 * @param {*} address 
 */
const addAddress = (address, openId) => {
    address.buyer_openid = openId 
    return model.add(ADDRESS,address)
}
const modifyAddress = (address, addressId) =>{
    return model.update(ADDRESS,address,addressId)
}
const delAddress = (addressId) =>{
  return model.remove(ADDRESS, addressId)
}
// 导出
module.exports = {
  getAddressList,
  resetAddress,
  setDefaultAddress,
  addAddress,
  getAddressById,
  modifyAddress,
  delAddress,
  resetAddress2,
  resetAddress
  

}
