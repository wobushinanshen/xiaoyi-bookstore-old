// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { ADMIN } = require('../config/tableConfig.js')

const { ADMINFIELD } = require('../fields/adminField.js')


/**
 * 获取首页轮播
 * @return 
 */
const getAdmin = () => {
   
    return model.query(ADMIN, ADMINFIELD, {} )

  // 导出
}
const updateAdmin = (admin, adminId) => {
  return model.update(ADMIN, admin, adminId)
}
  module.exports = {
    getAdmin,
    updateAdmin

  }
  