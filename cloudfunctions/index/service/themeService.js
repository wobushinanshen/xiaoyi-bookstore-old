// 引入BaseModel 集合名  字段过滤
const model = require('../models/BaseModel.js')
const { THEME } = require('../config/tableConfig.js')
const { ORDER } = require('../config/tableConfig.js')
const { THEMEFIELD } = require('../fields/themeField.js')

/**
 * 获取主题列表
 * @return 
 */
const getTheme = () => {
  return model.query( THEME, THEMEFIELD )
}
const addTheme = (theme) => {
  return model.add(THEME,theme)
}
const delTheme = (theme_id) => {
  return model.remove(THEME,theme_id)
}
const getThemeProduct = (theme_type) => {
  let options = {theme_type:theme_type}
  return model.query(THEME,THEMEFIELD,options)
}
module.exports = {
  getTheme,
  addTheme,
  delTheme,
  getThemeProduct,

  
}
