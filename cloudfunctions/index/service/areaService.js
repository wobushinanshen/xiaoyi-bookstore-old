// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { AREA } = require('../config/tableConfig.js')
// 返回字段处理
const { AREAFIELD } = require('../fields/areaField.js')
/**
 * 获取首页轮播
 * @return 
 */
const getAreaList = ()=>{
  // 查询需要前天显示的轮播
  // 传递参数   对应的BaseModel的方法名称
  return model.query(AREA, AREAFIELD, {} )
}
// 导出
module.exports = {
  getAreaList,
}
