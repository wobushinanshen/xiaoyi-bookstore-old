// 集合名 
module.exports = {
    BANNER : 'banner', 
    THEME  : 'theme',
    PRODUCT: 'product',
    PRODUCT_THEME : 'product_theme',
    PRODUCT_CATEGORY : 'product_category',
    ORDER:"order",
    ADMIN : 'admin',
    AREA : 'area',
    ADDRESS : 'address',
    NOTICE : 'notice',
    PRINTER : 'printer',
    SINGIN :'sign_in',
    CUSTOMER:'customer'
  }
  