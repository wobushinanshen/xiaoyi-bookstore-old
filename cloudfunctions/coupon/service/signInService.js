// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { SIGNIN, SINGIN } = require('../config/tableConfig.js')
// 返回字段处理
const { SIGNINFIELD } = require('../fields/signInField.js')
/**
 * 获取首页轮播
 * @return 
 */
const getCalendar = (calendar) => {
    return model.query(SINGIN,SIGNINFIELD,calendar)
   
}
const updateCalendar = (calendar, id) => {
    return model.update(SINGIN, calendar, id)
}
const addCalendar = (calendar) => {
    return model.add(SINGIN, calendar)
}
// 导出
module.exports = {
    getCalendar,
    updateCalendar,
    addCalendar,

}
