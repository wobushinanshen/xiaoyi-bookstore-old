// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router');
// 返回工具类
const returnUtil = require('utils/ReturnUtil.js')
// 商品信息业务层

const Coupon = require('service/couponService.js')
const SignIn = require('service/signInService.js')
cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: 'ibookshop-1wxto'
  
})
const IMAGEPREFIX = "cloud://ibookshop-1wxto.6962-ibookshop-1wxto-1303227463"

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const app = new TcbRouter({ event });
  // app.use 表示该中间件会适用于所有的路由
  app.use(async (ctx, next) => {
    ctx.data = {};
    await next(); // 执行下一中间件
  });
  /***************************    优惠券  *****************************************/    
 
  // 1、添加用户数据
  app.router('addCoupon',async (ctx,next) => {
    let coupon = event.data.coupon
    ctx.data = await Coupon.addCoupon(coupon)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 2、获取用户数据
  app.router('getCoupon',async (ctx,next) => {
    let coupon = event.data.coupon
    ctx.data = await Coupon.getCoupon(coupon)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 3、更新用户数据
  app.router('updateCoupon',async (ctx,next) => {
    let coupon = event.data.coupon
    let couponId = event.data.couponId
    ctx.data = await Coupon.updateCoupon(coupon, couponId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 4、删除用户数据
  app.router('delCoupon',async (ctx,next) => {
    let couponId = event.data.couponId
    ctx.data = await Coupon.delCoupon(couponId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  return app.serve();
}
